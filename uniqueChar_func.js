
/****************** uniqueChar function ****************************

Write a JavaScript function to extract unique characters from a string

********************************************************************/

var uniqueChars = function(string){
	var unique_arr = [];
	for(var i = 0; i < string.length; i++){
		// if character isn't same as it's succeeding character, and it's not present in list, and its not space character
		if((string[i] != string[i+1]) && (unique_arr.indexOf(string[i]) == -1) && (string[i] != " "))
			unique_arr.push(string[i]);
	}
	return unique_arr;
};

/******************** Test case ************************/

var test = function(result,expected_value){
	if(result.toString() === expected_value.toString()) {
		console.info("Test passed");
	}
	else {
		console.error("Test failed\n", "result: " + result + "\n", "expected value: " + expected_value);
	}
};

test(uniqueChars(""),[]);
test(uniqueChars("abcd"),['a','b','c','d']);
test(uniqueChars("aabaaabbaaaa"),['a','b']);
test(uniqueChars("here are some chars"),['h', 'e', 'r', 'a', 's', 'o', 'm', 'c']);
