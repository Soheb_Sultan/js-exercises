
/****************** findLongestWord function ****************************

Write a function filterLongWords that takes an array of words and an integer
i and returns the array of words that are longer than i.

 ************************************************************************/

 var filterLongWords = function(arr,integer){
	var filter_arr = [];
	for(var i = 0; i < arr.length; i++){
		if(arr[i].length > integer)
			filter_arr.push(arr[i]);
	}
	return filter_arr;
};

/******************** Test case ************************/

var test = function(result,expected_value){
	if(result.toString() === expected_value.toString()) {
		console.info("Test passed");
	}
	else {
		console.error("Test failed\n", "result: " + result + "\n", "expected value: " + expected_value);
	}
};

test(filterLongWords(['a','abd','abc'], 2),['abd', 'abc']);
test(filterLongWords(['asdaw','asdcdsed','sdcs'], 4),['asdaw','asdcdsed']);
test(filterLongWords(['asd','asd','qsd'], 4),[]);
