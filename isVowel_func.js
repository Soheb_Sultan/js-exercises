
/****************** isVowel function ************************************

Write a function that takes a character (i.e. a string of length 1) and returns
true if it is a vowel [a, e, i, o u], false otherwise

*********************************************************************/

var isVowel = function(char){
	var vowel = ['a','e','i','o','u'];
	char = char.toLowerCase();
	if(arguments.length == 1){
		for(var i = 0; i < vowel.length; i++){
			if(vowel[i] == char)
				return true;
		}
	}
	return false;
};

/******************** Test case ************************/

var test = function(result,expected_value){
	if(result === expected_value) {
		console.info("Test passed");
	}
	else {
		console.error("Test failed\n", "result: " + result + "\n", "expected value: " + expected_value);
	}
};

test(isVowel('a'),true);
test(isVowel('m'),false);
test(isVowel('U'),true);




