
/*************************** upperCase function **********************************

Write a JavaScript function that accepts a string as a parameter and converts the 
first letter of each word of the string in upper case.

**********************************************************************************/

var upperCase = function(word){
	if(typeof word == 'string'){
		var str_arr = word.split(" ");
		for(var i = 0; i < str_arr.length; i++){
			if(str_arr[i][0] != str_arr[i][0].toUpperCase()){
				str_arr[i] = str_arr[i].replace(str_arr[i][0],str_arr[i][0].toUpperCase());
			}
		}
		str_arr = str_arr.toString().replace(/,/g," ");
		return str_arr;
	}
	else return word.toString();
};

/******************** Test case ************************/

var test = function(result,expected_value){
	if(result === expected_value) {
		console.info("Test passed");
	}
	else {
		console.error("Test failed\n", "result: " + result + "\n", "expected value: " + expected_value);
	}
};

test(upperCase("hello"),"Hello");
test(upperCase("hello world"),"Hello World");
test(upperCase("something went wrong"),"Something Went Wrong");
test(upperCase("New York"),"New York");
test(upperCase(203),"203");





