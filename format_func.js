
/****************** format function **********************

Write  function which will format a value and return it. The value provided 
can be a string or number and the function always needs to return the number 
in X.XX format (to 2 decimal places).

*********************************************************/

var format = function(value){
	var formatted_value = 0;
	if(typeof value == 'number' || typeof value == 'string'){
		formatted_value = parseFloat(value);
		if(!isNaN(formatted_value)){
			formatted_value = formatted_value.toFixed(2);
			return formatted_value;
		}
		return "0.00";
	}
};

/******************** Test case ************************/

var test = function(result,expected_value){
	if(result === expected_value) {
		console.info("Test passed");
	}
	else {
		console.error("Test failed\n", "result: " + result + "\n", "expected value: " + expected_value);
	}
};

test(format(2),'2.00');
test(format("2"),'2.00');
test(format("2.00"),'2.00');
test(format("2.1234"),'2.12');
test(format(2.167),'2.17');
test(format("Moeez"),'0.00');
test(format("123.123"),'123.12');
test(format("I12"),'0.00');


