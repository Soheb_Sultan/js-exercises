
/****************** reverse function ******************************

Define a function reverse() that computes the reversal of a string.

*********************************************************************/

var reverse = function(string){
	var reverse_str = string.split("").reverse().join("");
	return reverse_str;
};

/******************** Test case ************************/

var test = function(result,expected_value){
	if(result === expected_value) {
		console.info("Test passed");
	}
	else {
		console.error("Test failed\n", "result: " + result + "\n", "expected value: " + expected_value);
	}
};

test(reverse("moeez"),"zeeom");
test(reverse("abcd"),"dcba");
test(reverse("jag testar"),"ratset gaj");
test(reverse("abba"),"abba");


