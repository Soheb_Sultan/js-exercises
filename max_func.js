
/****************** max function ************************************

Define a function max() that takes two numbers as arguments and returns 
the largest of them. Use the if-then-else construct available in Javascript.

*********************************************************************/

var max = function(val1,val2){
	if(typeof val1 && typeof val2 == 'number'){
		var compare = (val1 > val2) ? val1 : val2;
		return compare;
	}
	else return 0;
};

/******************** Test case ************************/

var test = function(result,expected_value){
	if(result === expected_value) {
		console.info("Test passed");
	}
	else {
		console.error("Test failed\n", "result: " + result + "\n", "expected value: " + expected_value);
	}
};

test(max(3,4),4);
test(max(24,2),24);4
test(max(100,100),100);
test(max(-100,100),100);
test(max("a","v"),0);


