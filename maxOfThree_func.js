
/****************** max function ************************************

Define a function maxOfThree() that takes three numbers as arguments 
and returns the largest of them.

*********************************************************************/

var maxOfThree = function(val1,val2,val3){
	if(typeof val1 && typeof val2 && typeof val3 == 'number'){
		var larger = (val1 > val2) ? val1 : val2;
		var largest = (val3 > larger) ?val3 : larger;
		return largest;
	}
	else return 0;
};

/******************** Test case ************************/

var test = function(result,expected_value){
	if(result === expected_value) {
		console.info("Test passed");
	}
	else {
		console.error("Test failed\n", "result: " + result + "\n", "expected value: " + expected_value);
	}
};

test(maxOfThree(1,2,3),3);
test(maxOfThree(12,232,123),232);
test(maxOfThree(-1,22,33),33);
test(maxOfThree(11, 11, 11),11);
test(maxOfThree("a","c","b"),0);