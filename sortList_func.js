
/****************** sum function ************************************

Write a function to sort a list of words (an array) in alphabetical order

*********************************************************************/

var sortList = function(arr){
	var sorted_arr = arr.sort();
	return sorted_arr;
};

/******************** Test case ************************/

var test = function(result,expected_value){
	if(result.toString() === expected_value.toString()) {
		console.info("Test passed");
	}
	else {
		console.error("Test failed\n", "result: " + result + "\n", "expected value: " + expected_value);
	}
};

test(sortList(["ab","fc","me","ec"]),["ab", "ec", "fc", "me"]);
test(sortList(["He", "programs", "in", "javascript."]),["He", "in", "javascript.", "programs"]);

