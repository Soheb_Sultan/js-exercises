
/****************** multiply function ************************************

Define a function multiply() that multiplies all the numbers in an array of numbers.

*********************************************************************/

var multiply = function(arr){
	var total = 1;
	for(var i = 0; i < arr.length; i++){
		if(typeof arr[i] == 'number')
			total *= arr[i];
		else return 0;
	}
	return total;
};

/******************** Test case ************************/

var test = function(result,expected_value){
	if(result === expected_value) {
		console.info("Test passed");
	}
	else {
		console.error("Test failed\n", "result: " + result + "\n", "expected value: " + expected_value);
	}
};

test(multiply([1,2,3,4]),24);
test(multiply([2,4,6,8]),384);





