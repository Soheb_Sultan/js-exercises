
/****************** findLongestWord function ****************************

Write a function findLongestWord that takes an array of words and returns
the length of the longest one.

 ************************************************************************/

var findLongestWord = function(arr){
	var longest_length = 0;
	for(var i = 0; i < arr.length; i++){
		if(arr[i].length > longest_length)
			longest_length = arr[i].length;
	}
	return longest_length;
};

/******************** Test case ************************/

var test = function(result,expected_value){
	if(result === expected_value) {
		console.info("Test passed");
	}
	else {
		console.error("Test failed\n", "result: " + result + "\n", "expected value: " + expected_value);
	}
};

test(findLongestWord(['a','ab','abc']),3);
test(findLongestWord(['asdaw','asdcdsed','sdcs']),8);
test(findLongestWord([]),0);

