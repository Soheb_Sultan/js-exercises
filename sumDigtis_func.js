
/*************************** isPalindrome function **********************************

Write a function named sumDigits which takes a number as input and returns the sum of 
the absolute value of each of the number's decimal digits.

************************************************************************************/

var sumDigits = function(value){
	var sum = 0;
	if(!isNaN(Number(value))){
		value = parseInt(value).toString();
		for(i = 0; i < value.length; i++){
			sum += parseInt(value[i]);
		}
	}
	return sum;
};

/******************** Test case ************************/

var test = function(result,expected_value){
	if(result === expected_value) {
		console.info("Test passed");
	}
	else {
		console.error("Test failed\n", "result: " + result + "\n", "expected value: " + expected_value);
	}
};

test(sumDigits(1234567),28);
test(sumDigits(1234567.34542),28);
test(sumDigits("1234567"),28);