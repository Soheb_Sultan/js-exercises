
/*************************** isPalindrome function **********************************

Write a JavaScript function that checks whether a passed string is palindrome or not?

************************************************************************************/

var isPalindrome = function(word){
	if(typeof word == 'string'){
		word = word.replace(/\s+/g,'');
		var str = word.split("").reverse().join("");
		if(str == word)
			return true;
		return false;
	}
	else if(typeof word == 'number'){
		var str = word.toString().split("").reverse().join("");
		if(str == word)
			return true;
		return false;
	}
};

/******************** Test case ************************/

var test = function(result,expected_value){
	if(result === expected_value) {
		console.info("Test passed");
	}
	else {
		console.error("Test failed\n", "result: " + result + "\n", "expected value: " + expected_value);
	}
};

test(isPalindrome("abba"),true);
test(isPalindrome(12321),true);
test(isPalindrome("moeez"),false);
test(isPalindrome("123421"),false);
test(isPalindrome("a santa at nasa"),true);
test(isPalindrome("a nut for a jar of tuna"),true);
test(isPalindrome("a lad named f mandala"),false);



